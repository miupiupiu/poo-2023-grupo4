package ar.edu.unju.fi.poo_2023_grupo4.service.imp;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.poo_2023_grupo4.dto.EmailDto;
import ar.edu.unju.fi.poo_2023_grupo4.service.EmailService;

@Service
public class EmailServiceImp implements EmailService {
	
	private static final Logger logger = Logger.getLogger(EmailServiceImp.class);
	
    private final JavaMailSender emailSender;
    @Autowired
    public EmailServiceImp(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    public String sendHtmlEmail(EmailDto email) throws MessagingException, MailSendException {
    	try {
    		logger.info("Se inicia el envío del mail");
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(email.getTo());
            helper.setSubject(email.getSubject());
            helper.setText(email.getBody(), true);
            emailSender.send(message);
            logger.info("Se envió el mail");
            return "Enviado";
    	}catch(MailSendException e ) {
    		logger.error("No enviado, error en el formato");
    		return "No enviado, error en el formato";
    	}
    	
    }
}

