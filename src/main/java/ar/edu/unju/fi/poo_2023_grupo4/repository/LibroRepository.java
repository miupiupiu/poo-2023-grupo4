package ar.edu.unju.fi.poo_2023_grupo4.repository;

import ar.edu.unju.fi.poo_2023_grupo4.entity.Libro;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LibroRepository extends CrudRepository<Libro,Integer> {
    boolean existsByIsbn(String isbn);
    boolean existsByNumInventario( Integer numInventario);

    List<Libro> findByAutor(String autor);
    List<Libro> findByTitulo(String titulo);
    List<Libro> findByIsbn(String isbn);
}
