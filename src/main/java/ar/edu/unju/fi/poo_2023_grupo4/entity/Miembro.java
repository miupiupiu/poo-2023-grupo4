package ar.edu.unju.fi.poo_2023_grupo4.entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.Table;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipoMiembro")
@Table(name = "miembros")
public class Miembro implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	private String nombre;
	
	@Column
	private Integer numeroMiembro = 0;
	
	@Column
	private String correo;
	
	@Column
	private String telefono;

	@Column(name = "fecha_hasta_sancion")
	private LocalDateTime fechaHastaSancion;
	@OneToMany(mappedBy = "miembro", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Prestamo> prestamos = new ArrayList<>();

	public Miembro() {
	}
	
	public Miembro(String nombre, String correo, String telefono) {
		super();
		this.nombre = nombre;
		this.correo = correo;
		this.telefono = telefono;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getNumeroMiembro() {
		return numeroMiembro;
	}

	public void setNumeroMiembro(Integer numeroMiembro) {
		this.numeroMiembro = numeroMiembro++;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public LocalDateTime getFechaHastaSancion() {
		return fechaHastaSancion;
	}

	public void setFechaHastaSancion(LocalDateTime fechaHastaSancion) {
		this.fechaHastaSancion = fechaHastaSancion;
	}

	public List<Prestamo> getPrestamos() {
		return prestamos;
	}

	public void setPrestamos(List<Prestamo> prestamos) {
		this.prestamos = prestamos;
	}
}
