package ar.edu.unju.fi.poo_2023_grupo4.service.imp;

import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unju.fi.poo_2023_grupo4.dto.DevolucionDto;
import ar.edu.unju.fi.poo_2023_grupo4.dto.EndPointsDto;
import ar.edu.unju.fi.poo_2023_grupo4.util.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.io.exceptions.IOException;

import ar.edu.unju.fi.poo_2023_grupo4.dto.EmailDto;
import ar.edu.unju.fi.poo_2023_grupo4.dto.PrestamoDto;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Libro;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Miembro;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Prestamo;
import ar.edu.unju.fi.poo_2023_grupo4.exception.ModelException;
import ar.edu.unju.fi.poo_2023_grupo4.repository.LibroRepository;
import ar.edu.unju.fi.poo_2023_grupo4.repository.MiembroRepository;
import ar.edu.unju.fi.poo_2023_grupo4.repository.PrestamoRepository;
import ar.edu.unju.fi.poo_2023_grupo4.service.EmailService;
import ar.edu.unju.fi.poo_2023_grupo4.service.PrestamoService;
import ar.edu.unju.fi.poo_2023_grupo4.strategy.PrestamoGeneradorResumenStrategy;
import jakarta.mail.MessagingException;


@Service
public class PrestamoServiceImp implements PrestamoService {

	private PrestamoRepository prestamoRepository;
    private PrestamoMapper prestamoMapper;
    private LibroRepository libroRepository;
    private EmailService emailService;
    private MiembroRepository miembroRepository;
    private DateTimeFormatter formatter;
    private PrestamoGeneradorResumenStrategy resumen;
    private PDFGenerator pdfGenerator;
    private DevolucionMapper devolucionMapper;
	private static final Logger logger = Logger.getLogger(PrestamoServiceImp.class);
    
    @Autowired
    public PrestamoServiceImp(PrestamoRepository prestamoRepository, PrestamoMapper prestamoMapper, LibroRepository libroRepository, EmailService emailService, MiembroRepository miembroRepository, PDFGenerator pdfGenerator, DevolucionMapper devolucionMapper){
        this.prestamoMapper= prestamoMapper;
        this.prestamoRepository = prestamoRepository;
        this.libroRepository = libroRepository;
        this.emailService = emailService;
        this.miembroRepository = miembroRepository;
        formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        this.pdfGenerator = pdfGenerator;
        this.devolucionMapper = devolucionMapper;
    }

    @Override
    public PrestamoDto crearPrestamo(EndPointsDto endPointsDto) throws ModelException, MessagingException {
        logger.info("Se inició la creacion del prestamo");
        Prestamo prestamo = new Prestamo();
        completarPrestamo(endPointsDto, prestamo);
        logger.info("Se completo la sancion");
        Miembro miembro = miembroRepository.findById(endPointsDto.getMiembroId()).orElseThrow(()-> new ModelException("Miembro no encontrado"));
        verificarSancion(miembro);
        logger.info("Se verifico que el miembro tenga o no sancion");
        Libro libro = libroRepository.findById(endPointsDto.getLibroId()).orElseThrow(()-> new ModelException("Libro inexistente"));
        prestamo.setMiembro(miembro);
        procesarPrestamo(prestamo, libro);
        logger.info("Se realizo un prestamo a esta hora");
        return prestamoMapper.convertirAPrestamoDto(prestamo);
    }

    private void completarPrestamo(EndPointsDto endPointsDto, Prestamo prestamo){
        logger.info("Se setearan la fecha y hora");
    	LocalDateTime fechaDeHoy = LocalDateTime.now();
        if(endPointsDto.getFechaPrestamo()==null || endPointsDto.getFechaPrestamo().isBlank()){
        	prestamo.setFechaPrestamo(fechaDeHoy.withNano(0));
        }
        else {
            prestamo.setFechaPrestamo(LocalDateTime.parse(endPointsDto.getFechaPrestamo(), formatter));
        }
        if(endPointsDto.getFechaDevolucion()==null || endPointsDto.getFechaDevolucion().isBlank()){
            prestamo.setFechaDevolucion(fechaDeHoy.plusDays(2).withNano(0));
        }
        else {
            prestamo.setFechaDevolucion(LocalDateTime.parse(endPointsDto.getFechaDevolucion(), formatter));
        }
        logger.debug("Fecha de prestamo "+prestamo.getFechaPrestamo().format(formatter));
        logger.debug("Fecha de devolucion "+prestamo.getFechaDevolucion().format(formatter));
        logger.info("Se setearon las fechas");
    }

    private void verificarSancion(Miembro miembro) throws ModelException {
    	logger.info("Se verificara si se posee sancion");
        long diasDeDiferencia=0;
        if(miembro.getFechaHastaSancion()!=null)
            diasDeDiferencia = ChronoUnit.DAYS.between(miembro.getFechaHastaSancion(), LocalDateTime.now());
        if(diasDeDiferencia < 0){
        	logger.error("El miembro esta sancionado hasta "+miembro.getFechaHastaSancion().format(formatter));
            throw new ModelException("El miembro esta sancionado hasta: " + miembro.getFechaHastaSancion().format(formatter));
        }
    }

    private void procesarPrestamo(Prestamo prestamo, Libro libro) throws ModelException, MessagingException {
       logger.info("Se verifica el estado del libro");
    	if (libro.getEstadoLibro().compareTo(EstadoLibro.DISPONIBLE) == 0) {
            realizarPrestamo(prestamo, libro);
        } else {
            manejarEstadoLibro(libro);
        }
    }

    private void realizarPrestamo(Prestamo prestamo, Libro libro) throws MessagingException {
        logger.info("Se realizara el prestamo");
    	libro.setEstadoLibro(EstadoLibro.PRESTADO);
        prestamo.setEstadoPrestamo(EstadoPrestamo.PRESTADO);
        prestamo.setLibro(libro);
        logger.debug("Se seteraon estos datos para realizar el prestamo [Libro] -> estado "+libro.getEstadoLibro().toString()+" [Prestamo]  ->prestamo "+prestamo.getEstadoPrestamo().toString()+" -> libro id del prestamo "+prestamo.getLibro().getId());
        var pdfUrl = pdfGenerator.generarPDF(prestamo);
        prestamo.setPdfUrl(pdfUrl);
        logger.debug("Se almaceno el pdf en la siguiente ruta "+prestamo.getPdfUrl());
        prestamo = prestamoRepository.save(prestamo);
        logger.debug("Se realizo el prestamo con el id "+prestamo.getId());
        libroRepository.save(libro);
        logger.debug("Se actualizo el libro con el id "+libro.getId());
        logger.debug(prestamo.getMiembro().getNombre() + " registró un préstamo por el libro " + prestamo.getLibro().getTitulo());
        emailService.sendHtmlEmail(crearEmail(prestamo));
        logger.info("Se realizo el prestamo");
    }

    private void manejarEstadoLibro(Libro libro) throws ModelException {
        if (libro.getEstadoLibro().compareTo(EstadoLibro.BAJA) == 0) {
            logger.error("Libro dado de Baja");
            throw new ModelException("Libro Dado de Baja");
        } else {
            logger.error("Libro Prestado");
            throw new ModelException("Libro Prestado");
        }
    }

    private EmailDto crearEmail(Prestamo prestamo){
    	logger.info("Se inició la creacion del email");
        EmailDto email = new EmailDto();
        email.setTo(prestamo.getMiembro().getCorreo());
        email.setSubject("Prestamo Realizado con Exito");
        email.setBody(Recibo.html(prestamo));
        return email;
    }

	@Override
	public DevolucionDto devolverPrestamo(int id) throws ModelException {
		logger.info("Se inició la devolución del Prestamo");
        var prestamoDto = this.obtenerPrestamoPorId(id);
		completarDevolucion(prestamoDto);
		logger.debug("El miembro: "+prestamoDto.getMiembro().getNombre()+" devuelve el libro "+prestamoDto.getLibro().getTitulo()+" el dia "+prestamoDto.getFechaDevolucionPorMiembro());
		Prestamo prestamo = prestamoMapper.convertirAPrestamo(prestamoDto);
		Libro libro =  libroRepository.findById(prestamo.getLibro().getId()).orElseThrow(()-> new ModelException("Fallo al encontrar el libro"));
        Miembro miembro = miembroRepository.findById(prestamoDto.getMiembro().getId()).orElseThrow(()-> new ModelException("Miembro no encontrado"));
        prestamo.setMiembro(miembro);
        procesarDevolucion(prestamo, libro);
        verificarFechaDeDevolucionYSancionar(prestamo);
        return devolucionMapper.convertirADevolucionDto(prestamo);
	}


    private void completarDevolucion(PrestamoDto prestamoDto) {
		LocalDateTime fechaDevolucion = LocalDateTime.now();
		logger.debug("La fecha de la devolucion es "+ fechaDevolucion);
		logger.debug("El formato que se manda para la fecha es "+formatter);
        prestamoDto.setFechaDevolucionPorMiembro(fechaDevolucion.format(formatter));
        logger.debug("Fecha seteada "+prestamoDto.getFechaDevolucionPorMiembro());

	}

    private void procesarDevolucion(Prestamo prestamo, Libro libro) throws ModelException {
        if (libro.getEstadoLibro() != EstadoLibro.PRESTADO) {
            logger.error("No se pudo realizar la devolución, contactese con mesa de ayuda");
            throw new ModelException("No se pudo realizar la devolución, contactese con mesa de ayuda");
        }
        realizarDevolucion(prestamo, libro);
    }

    private void realizarDevolucion(Prestamo prestamo, Libro libro) {
        libro.setEstadoLibro(EstadoLibro.DISPONIBLE);
        prestamo.setEstadoPrestamo(EstadoPrestamo.DEVUELTO);
        prestamo.setLibro(libro);
        logger.debug("Se seteraon estos datos para la devolucion [Libro] -> estado "+libro.getEstadoLibro().toString()+" [Prestamo]  ->prestamo "+prestamo.getEstadoPrestamo().toString()+" -> libro id del prestamo "+prestamo.getLibro().getId());
        prestamoRepository.save(prestamo);
        libroRepository.save(libro);
        logger.debug("Se actualizaron los datos del prestamo con este id "+prestamo.getId());
        logger.debug("Se actualizaron los datos del libro con este id "+libro.getId());
    }

    private void verificarFechaDeDevolucionYSancionar(Prestamo prestamo) {
        LocalDateTime fechaDeHoy = LocalDateTime.now();
        long diasDeDiferencia = ChronoUnit.DAYS.between(prestamo.getFechaDevolucion(), prestamo.getFechaDevolucionPorMiembro());
        LocalDateTime fechaSancion = calcularFechaSancion(fechaDeHoy, diasDeDiferencia);
        if(fechaSancion != null) {
            logger.debug("La fehca de sancion es hasta "+fechaSancion.format(formatter));
        	prestamo.getMiembro().setFechaHastaSancion(fechaSancion.withNano(0));
            miembroRepository.save(prestamo.getMiembro());
        }
    }

    private LocalDateTime calcularFechaSancion(LocalDateTime fechaDeHoy, long diasDeDiferencia) {
        if (diasDeDiferencia >= 1 && diasDeDiferencia <= 2) {
            return fechaDeHoy.plusDays(3);
        } else if (diasDeDiferencia >= 3 && diasDeDiferencia <= 5) {
            return fechaDeHoy.plusDays(5);
        } else if (diasDeDiferencia > 5) {
            return fechaDeHoy.plusDays(20);
        } else {
            return null;
        }
    }

    @Override
	public PrestamoDto obtenerPrestamoPorId(Integer i) throws ModelException {
		var prestamo = prestamoRepository.findById(i).orElseThrow(()-> new ModelException("El id: " + i + " no existe"));
		return prestamoMapper.convertirAPrestamoDto(prestamo);
	}
    
    @Override
	public ByteArrayOutputStream crearResumenPrestamos(String desde, String hasta) throws ModelException, IOException, java.io.IOException{
		LocalDateTime desdeFecha = LocalDateTime.parse(desde,formatter);
		logger.debug("Fecha desde: "+desdeFecha.toString());
		LocalDateTime hastaFecha = LocalDateTime.parse(hasta,formatter);
		logger.debug("Fecha hasta: "+hastaFecha.toString());
		List<PrestamoDto> prestamos = this.devolverPrestamoPorFechas(desdeFecha, hastaFecha);
		return resumen.generarResumen(prestamos);
	}
    private List<PrestamoDto> devolverPrestamoPorFechas(LocalDateTime desde, LocalDateTime hasta) throws ModelException {
        Iterable<Prestamo> prestamos = prestamoRepository.findAll();
        if (prestamos == null) {
            throw new ModelException("La lista de préstamos es nula.");
        }
        List<PrestamoDto> prestamosFiltrados = new ArrayList<>();
        for(Prestamo p : prestamos) {
        	LocalDateTime fechaPrestamo = p.getFechaPrestamo();
            if ((fechaPrestamo.isEqual(desde) || fechaPrestamo.isAfter(desde)) &&
                (fechaPrestamo.isEqual(hasta) || fechaPrestamo.isBefore(hasta))) {
                prestamosFiltrados.add(prestamoMapper.convertirAPrestamoDto(p));
            }
        }
       if (prestamosFiltrados.isEmpty()) {
    	   logger.error("No se encontraron préstamos en el rango especificado");
          throw new ModelException("No se encontraron préstamos en el rango especificado.");
       }
       return prestamosFiltrados;
    }

	@Override
	public void setResumenStrategy(PrestamoGeneradorResumenStrategy resumenStrategy) {
		this.resumen = resumenStrategy;
	}
}


