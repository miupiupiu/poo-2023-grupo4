package ar.edu.unju.fi.poo_2023_grupo4.util;

public enum EstadoPrestamo {
    PRESTADO,
    DEVUELTO
}
