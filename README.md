# POO-2023-GRUPO4

## Trabajo Final 
Este trabajo fue realizado para la materia Programación Orientada a Objetos de la carrera de Analista Programador Universitario

### Integrantes
- Fernández, José Manuel
- Guerrero, Rocio de los Angeles

## Nombre del Proyecto
POO-2023-GRUPO4


## Descripción
- Este proyecto tiene por finalidad la creación de un sistema para biblioteca donde las funcionalidades principales se centran en la gestión de préstamos de libros a miembros (docentes y alumnos)

- Diagrama UML del Proyecto

![UML](Diagrama Proyecto Final.png)

## Instalación
### Requisitos Previos
- **Versión de Java de Base puede ser la número :** 17
    - Verifica la instalación con el siguiente comando:
        ```bash
        java --version
        ```
        La respuesta esperada es:
        ```
        openjdk 21.0.1 2023-10-17 LTS
        OpenJDK Runtime Environment Temurin-21.0.1+12 (build 21.0.1+12-LTS)
        OpenJDK 64-Bit Server VM Temurin-21.0.1+12 (build 21.0.1+12-LTS, mixed mode, sharing)
        ```
        Si la versión no es correcta, instala OpenJDK 17 o superiores desde [adoptium.net](https://adoptium.net/es/download/).

- **IDE de Java:**
    - Se recomienda usar:
        - Eclipse IDE  [Descargue aquí](https://www.eclipse.org/downloads/packages/)
        - IntelliJ IDEA (cualquiera de sus versiones) [Descargue aquí](https://www.jetbrains.com/idea/download/?source=google&medium=cpc&campaign=AMER_en_AMER_IDEA_Branded&term=intellij+idea&content=602143185580&gad=1&gclid=CjwKCAiA9ourBhAVEiwA3L5RFqxQDHwLiPN_rXNq4HjssI1fg99U4maoA4YK2gERm87h3X7tg-BUYRoCpksQAvD_BwE&section=windows)

- **Base de Datos MySQL:**
    - Se recomienda utilizar MySQL Workbench.
    - Nombre de la base de datos: 'poo2023'

- **Lector de archivos .pdf y .xls:**
    - Se recomienda el uso de cualquier navegador para los PDF y Microsoft Excel para los archivos xls

- **Aplicación de escritorio para API:**
    - Se recomienda Insomnia, una aplicación de escritorio multiplataforma gratuita para simplificar la interacción y el diseño de API basadas en HTTP.

### Pasos de Instalación
1. Descarga el proyecto y ábrelo en una IDE válida.
2. Crea la base de datos 'poo2023' en MySQL.
3. Ejecuta la clase 'Poo2023Grupo4Application' ubicada en 'ar.edu.unju.fi.poo_2023_grupo4'.
4. Abre Insomnia o la aplicación que prefieras para interactuar con las API.

## Uso
1. **Descargar el Proyecto:**
    - Clona o descarga el proyecto desde el repositorio.

2. **Base de Datos:**
    - Crea la base de datos 'poo2023' en MySQL. Puedes utilizar MySQL Workbench para este paso.

3. **Ejecutar la Aplicación:**
    - Corre la clase 'Poo2023Grupo4Application' ubicada en 'ar.edu.unju.fi.poo_2023_grupo4'.

4. **Interactuar con las API:**
    - Abre Insomnia o la aplicación de tu elección.
    - Utiliza el URL y el método según el controlador ubicado en 'ar.edu.unju.fi.poo_2023_grupo4.controller' para desplegar las funcionalidades.


