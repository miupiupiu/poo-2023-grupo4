package ar.edu.unju.fi.poo_2023_grupo4.dto;

public class AlumnoDto extends MiembroDto {
	private static final long serialVersionUID = 1L;
	private String lu;

	public String getLu() {
		return lu;
	}
	public void setLu(String lu) {
		this.lu = lu;
	}
	
}
