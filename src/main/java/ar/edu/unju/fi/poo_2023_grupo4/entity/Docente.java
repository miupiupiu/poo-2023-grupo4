package ar.edu.unju.fi.poo_2023_grupo4.entity;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity
@DiscriminatorValue(value="Docente")
public class Docente extends Miembro {
	private static final long serialVersionUID = 1L;
	@Column
	private String legajo;

	
	public Docente() {
		super();
	}
	
	public Docente(String nombre, String correo, String telefono, String legajo) {
		super(nombre, correo, telefono);
		this.legajo = legajo;
	}
	
	public String getLegajo() {
		return legajo;
	}

	public void setLegajo(String legajo) {
		this.legajo = legajo;
	}
}
