package ar.edu.unju.fi.poo_2023_grupo4.service;

import java.io.ByteArrayOutputStream;

import ar.edu.unju.fi.poo_2023_grupo4.dto.DevolucionDto;
import ar.edu.unju.fi.poo_2023_grupo4.dto.EndPointsDto;
import com.itextpdf.io.exceptions.IOException;

import ar.edu.unju.fi.poo_2023_grupo4.dto.PrestamoDto;
import ar.edu.unju.fi.poo_2023_grupo4.exception.ModelException;
import ar.edu.unju.fi.poo_2023_grupo4.strategy.PrestamoGeneradorResumenStrategy;
import jakarta.mail.MessagingException;
public interface PrestamoService {
    PrestamoDto crearPrestamo(EndPointsDto endPointsDto) throws ModelException, MessagingException;
	DevolucionDto devolverPrestamo(int id) throws ModelException, MessagingException;
	PrestamoDto obtenerPrestamoPorId(Integer i) throws ModelException;
	ByteArrayOutputStream crearResumenPrestamos(String desde, String hasta) throws ModelException, IOException, java.io.IOException;
	void setResumenStrategy(PrestamoGeneradorResumenStrategy resumenStrategy);
}
