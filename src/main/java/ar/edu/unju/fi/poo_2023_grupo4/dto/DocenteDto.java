package ar.edu.unju.fi.poo_2023_grupo4.dto;

public class DocenteDto extends MiembroDto {
	private static final long serialVersionUID = 1L;
	private String legajo;
	
	public String getLegajo() {
		return legajo;
	}
	public void setLegajo(String legajo) {
		this.legajo = legajo;
	}
}
