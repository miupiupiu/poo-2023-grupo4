package ar.edu.unju.fi.poo_2023_grupo4.repository;

import ar.edu.unju.fi.poo_2023_grupo4.entity.Prestamo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrestamoRepository extends CrudRepository<Prestamo,Integer> {
}
