package ar.edu.unju.fi.poo_2023_grupo4.util;

import ar.edu.unju.fi.poo_2023_grupo4.dto.DevolucionDto;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Prestamo;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class DevolucionMapper {
    private final ModelMapper modelMapper;

    public DevolucionMapper() {
        this.modelMapper = new ModelMapper();
        this.modelMapper.getConfiguration().setSkipNullEnabled(true);
        this.setConverters();
    }

    private void setConverters() {
        Converter<LocalDateTime, String> fechasStringConverter = context -> {
            LocalDateTime source = context.getSource();
            if (source == null) {
                return null;
            }
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            return source.format(formatter);
        };
        modelMapper.createTypeMap(Prestamo.class, DevolucionDto.class)
                .addMappings(mapping -> {
                    mapping.map(Prestamo::getEstadoPrestamo, DevolucionDto::setEstadoPrestamo);
                    mapping.using(fechasStringConverter).map(Prestamo::getFechaPrestamo, DevolucionDto::setFechaPrestamo);
                    mapping.using(fechasStringConverter).map(Prestamo::getFechaDevolucion, DevolucionDto::setFechaDevolucion);
                    mapping.using(fechasStringConverter).map(Prestamo::getFechaDevolucionPorMiembro, DevolucionDto::setFechaDevolucionPorMiembro);
                    mapping.map(Prestamo::getEstadoPrestamo, DevolucionDto::setEstadoPrestamo);
                    mapping.map(src->src.getMiembro().getNombre(), (DevolucionDto dest, String value) -> dest.getMiembroDevolucionDto().setNombre(value));
                    mapping.using(fechasStringConverter).map(src->src.getMiembro().getFechaHastaSancion(), (DevolucionDto dest, String value) -> dest.getMiembroDevolucionDto().setFechaHastaSancion(value));
                    //libro titulo
                    mapping.map(src->src.getLibro().getTitulo(), (DevolucionDto dest, String value) -> dest.getLibroDevolucionDto().setTitulo(value));
                    //libro autor
                    mapping.map(src->src.getLibro().getAutor(), (DevolucionDto dest, String value) -> dest.getLibroDevolucionDto().setAutor(value));
                    //libro numInv
                    mapping.map(src->src.getLibro().getNumInventario(), (DevolucionDto dest, Integer value) -> dest.getLibroDevolucionDto().setNumInventario(value));
                });
    }

    public DevolucionDto convertirADevolucionDto(Prestamo prestamo) {
        return modelMapper.map(prestamo, DevolucionDto.class);
    }

}
