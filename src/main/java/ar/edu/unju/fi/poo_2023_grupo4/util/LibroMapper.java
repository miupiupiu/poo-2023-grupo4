package ar.edu.unju.fi.poo_2023_grupo4.util;

import ar.edu.unju.fi.poo_2023_grupo4.dto.LibroDto;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Libro;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class LibroMapper {
    private final ModelMapper modelMapper;

    public LibroMapper() {
        this.modelMapper = new ModelMapper();
        Converter<String, EstadoLibro> estadoLibroConverter = context -> EstadoLibro.valueOf(context.getSource().toUpperCase());
        modelMapper.createTypeMap(LibroDto.class, Libro.class).addMappings(mapping -> mapping.using(estadoLibroConverter).map(LibroDto::getEstadoLibro, Libro::setEstadoLibro));
        modelMapper.createTypeMap(Libro.class, LibroDto.class).addMappings(mapping -> mapping.map(Libro::getEstadoLibro, LibroDto::setEstadoLibro));
    }

    public LibroDto convertirALibroDTO(Libro libro) {
        return modelMapper.map(libro, LibroDto.class);
    }

    public Libro convertirALibro(LibroDto libroDto) {
        return modelMapper.map(libroDto, Libro.class);
    }
}
