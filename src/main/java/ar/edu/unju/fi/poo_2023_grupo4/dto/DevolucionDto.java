package ar.edu.unju.fi.poo_2023_grupo4.dto;

import java.io.Serializable;

public class DevolucionDto implements Serializable {
    private static final long serialVersionUID = 1L;
	public int id;
    public MiembroDevolucionDto miembroDevolucionDto;
    public LibroDevolucionDto libroDevolucionDto;
    private String fechaPrestamo;
    private String fechaDevolucion;
    private String fechaDevolucionPorMiembro;
    private String estadoPrestamo;
    private String pdfUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MiembroDevolucionDto getMiembroDevolucionDto() {
        return miembroDevolucionDto;
    }

    public void setMiembroDevolucionDto(MiembroDevolucionDto miembroDevolucionDto) {
        this.miembroDevolucionDto = miembroDevolucionDto;
    }

    public LibroDevolucionDto getLibroDevolucionDto() {
        return libroDevolucionDto;
    }

    public void setLibroDevolucionDto(LibroDevolucionDto libroDevolucionDto) {
        this.libroDevolucionDto = libroDevolucionDto;
    }

    public String getFechaPrestamo() {
        return fechaPrestamo;
    }

    public void setFechaPrestamo(String fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }

    public String getFechaDevolucion() {
        return fechaDevolucion;
    }

    public void setFechaDevolucion(String fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    public String getFechaDevolucionPorMiembro() {
        return fechaDevolucionPorMiembro;
    }

    public void setFechaDevolucionPorMiembro(String fechaDevolucionPorMiembro) {
        this.fechaDevolucionPorMiembro = fechaDevolucionPorMiembro;
    }

    public String getEstadoPrestamo() {
        return estadoPrestamo;
    }

    public void setEstadoPrestamo(String estadoPrestamo) {
        this.estadoPrestamo = estadoPrestamo;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }
}
