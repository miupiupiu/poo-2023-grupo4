package ar.edu.unju.fi.poo_2023_grupo4.service;

import ar.edu.unju.fi.poo_2023_grupo4.dto.LibroDto;
import ar.edu.unju.fi.poo_2023_grupo4.exception.ModelException;

import java.util.List;

public interface LibroService {
    /**
     * Crea un Libro en la base de datos
     * @param libroDto
     * @return Libro
     * @throws LibroException
     */
    LibroDto crearLibro(LibroDto libroDto) throws ModelException;

    boolean verificarExistenciaPorIsbn(String isbn);
    boolean verificarExistenciaPorNumInventario(Integer numInventario);

    void eliminarLibro(Integer id) throws ModelException;
    boolean verificarExistenciaPorId(Integer id);

    LibroDto modificarLibro(LibroDto libroDto) throws ModelException;

    LibroDto obtenerLibroPorId(Integer id) throws ModelException;

    List<LibroDto> buscarLibrosPorAutor(String autor);
    List<LibroDto> buscarLibrosPorTitulo(String titulo);
    List<LibroDto> buscarLibrosPorIsbn(String isbn);

}
