package ar.edu.unju.fi.poo_2023_grupo4.util;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Configuration;

import ar.edu.unju.fi.poo_2023_grupo4.dto.AlumnoDto;
import ar.edu.unju.fi.poo_2023_grupo4.dto.DocenteDto;
import ar.edu.unju.fi.poo_2023_grupo4.dto.MiembroDto;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Alumno;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Docente;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Miembro;

@Configuration
public class MiembroMapper {
	private final ModelMapper modelMapper;
	public MiembroMapper() {
		this.modelMapper = new ModelMapper();
	}
	
	public DocenteDto convertirADocenteDto(Miembro miembro) {
		return modelMapper.map(miembro, DocenteDto.class);
	}
	public AlumnoDto convertirAAlumnoDto(Miembro miembro) {
		return modelMapper.map(miembro, AlumnoDto.class);
	}
	
	public Docente convertirADocente(MiembroDto miembroDto) {
		return modelMapper.map(miembroDto, Docente.class);
	}
	public Alumno convertirAAlumno(MiembroDto miembroDto) {
		return modelMapper.map(miembroDto, Alumno.class);
	}

	public MiembroDto convertirAMiembroDto(Miembro miembro) {
		return modelMapper.map(miembro, MiembroDto.class);
	}
}
