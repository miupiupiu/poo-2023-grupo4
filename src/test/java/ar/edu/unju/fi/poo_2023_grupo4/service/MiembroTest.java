package ar.edu.unju.fi.poo_2023_grupo4.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.unju.fi.poo_2023_grupo4.dto.AlumnoDto;
import ar.edu.unju.fi.poo_2023_grupo4.dto.DocenteDto;
import ar.edu.unju.fi.poo_2023_grupo4.exception.ModelException;

@SpringBootTest
public class MiembroTest {
    @Autowired
    MiembroService miembroService;

    static AlumnoDto alumno;
    static DocenteDto docente;
    static AlumnoDto alumno1;
    static DocenteDto docente1;
    static DocenteDto docente2;
    static AlumnoDto alumno2;

    @BeforeAll
    static void setUp() {
        alumno = new AlumnoDto();
        alumno.setNombre("Jose Fernandez");
        alumno.setCorreo("jose@gmail.com");
        alumno.setLu("4534");
        alumno.setNumeroMiembro(1234);
        
        docente = new DocenteDto();
        docente.setNombre("Sara Solari");
        docente.setCorreo("sara@gmail.com");
        docente.setLegajo("A15");
        docente.setNumeroMiembro(1596);
        
        alumno1 = new AlumnoDto();
        alumno2 = new AlumnoDto();
        docente1 = new DocenteDto();
        docente2 = new DocenteDto(); 
    }

    @AfterAll
    static void tearDown() {
        alumno = null;
        alumno1 = null;
        alumno2 = null;
        docente = null;
        docente1 = null;
        docente2 = null;
    }
    
    @Test
    void crearMiembroTest() throws ModelException {
    	miembroService.crearMiembro(alumno);
    	miembroService.crearMiembro(docente);
    	assertNotNull(miembroService.validarExistenciaMiembro(alumno));
    	assertNotNull(miembroService.validarExistenciaMiembro(docente));
    }
   
    @Test
    void modificarAlumnoTest() throws ModelException {
    	alumno1 = (AlumnoDto) miembroService.buscarMiembroPorEmail("pauldoe@email.com");
    	assertNotNull(alumno1);
    	alumno1.setNombre("Solange Rios");
    	alumno1.setNumeroMiembro(879);
    	miembroService.modificarMiembro(alumno1);
    	assertNotNull(miembroService.buscarMiembroPorNumeroMiembro(alumno1.getNumeroMiembro()));
    }
    
    @Test
    void modificarDocenteTest() throws ModelException {
    	docente1 = (DocenteDto) miembroService.buscarMiembroPorEmail("jarana@gmail.com");
    	assertNotNull(docente1);
    	docente1.setNombre("Macareno Sosa");
    	docente1.setNumeroMiembro(25679);
    	miembroService.modificarMiembro(docente1);
    	assertNotNull(miembroService.buscarMiembroPorNumeroMiembro(docente1.getNumeroMiembro()));
    }
    
    @Test
    void eliminarAlumnoTest() throws ModelException {
    	alumno2 = (AlumnoDto) miembroService.buscarMiembroPorEmail("juariu@gmail.com");
    	assertNotNull(alumno2);
    	miembroService.eliminarMiembro(alumno2);
    	assertFalse(miembroService.validarMiembro(alumno2));
    }
    @Test
    void eliminarDocenteTest() throws ModelException {
    	docente2 = (DocenteDto) miembroService.buscarMiembroPorEmail("carolina@gmail.com");;
    	assertNotNull(docente2);
    	miembroService.eliminarMiembro(docente2);
    	assertFalse(miembroService.validarMiembro(docente2));
    }
}