package ar.edu.unju.fi.poo_2023_grupo4.util;

import ar.edu.unju.fi.poo_2023_grupo4.entity.Prestamo;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;

@Service
public class PDFGenerator {
    private final String filePath = "C:\\pdf-output";
    private static final Logger logger = Logger.getLogger(PDFGenerator.class);

    public String generarPDF(Prestamo prestamo) {
        try {
            File path = new File(filePath);
            if(!path.exists()){
                path.mkdir();
            }
            File file = new File(path + "\\" + prestamo.getMiembro().getCorreo() + "-" + prestamo.getFechaPrestamo().toString().replace(":", "") + ".pdf");
            PdfWriter writer = new PdfWriter(file);
            PdfDocument pdf = new PdfDocument(writer);
            ConverterProperties converterProperties = new ConverterProperties();
            HtmlConverter.convertToPdf(Recibo.html(prestamo), pdf,converterProperties);
            pdf.close();
            logger.info("PDF generado exitosamente en: " + filePath);
            return file.getAbsolutePath();
        } catch (FileNotFoundException e) {
            logger.error("PDF no generado - " + e.getMessage());
            return "0";
        }
    }
}
