package ar.edu.unju.fi.poo_2023_grupo4.entity;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity
@DiscriminatorValue(value="Alumno")
public class Alumno extends Miembro {
	private static final long serialVersionUID = 1L;
	@Column
	private String lu;
	
	public Alumno() {
		super();
	}
	
	public Alumno(String nombre, String correo, String telefono, String lu) {
		super(nombre, correo, telefono);
		this.lu = lu;
	}
	public String getLu() {
		return lu;
		}
	public void setLu(String lu) {
		this.lu = lu;
	}
}
