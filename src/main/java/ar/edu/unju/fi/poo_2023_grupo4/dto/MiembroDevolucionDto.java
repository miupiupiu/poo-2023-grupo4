package ar.edu.unju.fi.poo_2023_grupo4.dto;

import java.io.Serializable;

public class MiembroDevolucionDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private String nombre;
    private String fechaHastaSancion;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaHastaSancion() {
        return fechaHastaSancion;
    }

    public void setFechaHastaSancion(String fechaHastaSancion) {
        this.fechaHastaSancion = fechaHastaSancion;
    }
}
