package ar.edu.unju.fi.poo_2023_grupo4.dto;

import java.io.Serializable;

public class LibroDevolucionDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private String titulo;
    private String autor;
    private int numInventario;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getNumInventario() {
        return numInventario;
    }

    public void setNumInventario(int numInventario) {
        this.numInventario = numInventario;
    }
}
