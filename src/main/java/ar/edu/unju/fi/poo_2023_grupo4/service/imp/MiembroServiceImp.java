package ar.edu.unju.fi.poo_2023_grupo4.service.imp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ar.edu.unju.fi.poo_2023_grupo4.exception.ModelException;
import ar.edu.unju.fi.poo_2023_grupo4.repository.MiembroRepository;
import ar.edu.unju.fi.poo_2023_grupo4.dto.AlumnoDto;
import ar.edu.unju.fi.poo_2023_grupo4.dto.DocenteDto;
import ar.edu.unju.fi.poo_2023_grupo4.dto.MiembroDto;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Alumno;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Docente;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Miembro;
import ar.edu.unju.fi.poo_2023_grupo4.service.MiembroService;
import ar.edu.unju.fi.poo_2023_grupo4.util.MiembroMapper;

@Service
public class MiembroServiceImp implements MiembroService {
	@Autowired
	private MiembroRepository miembroRepository;
	@Autowired
	private MiembroMapper miembroMapper;
	
	private static final Logger logger = Logger.getLogger(MiembroServiceImp.class);
	
	public MiembroServiceImp(MiembroRepository miembroRepository, MiembroMapper miembroMapper) {
		this.miembroRepository = miembroRepository;
		this.miembroMapper=miembroMapper;
	}
	/**
	 * Crea Miembro tanto para Alumno como Docente
	 */
	@Override
	public void crearMiembro(MiembroDto miembro) throws ModelException {
		validarRegistroMiembro(miembro);
	    if (miembro instanceof AlumnoDto) {
	    	Alumno alumnoEntidad =  miembroMapper.convertirAAlumno(miembro);
	        alumnoEntidad = validarLU(alumnoEntidad);
	    	logger.debug("Alumno a almacenar "+alumnoEntidad.getNombre()+" con correo "+alumnoEntidad.getCorreo());
	        miembroRepository.save(alumnoEntidad);
	        logger.info("Se guardo un alumno en la Base de Datos");
	    } else {
	        Docente docenteEntidad = miembroMapper.convertirADocente(miembro);
	        docenteEntidad = validarLegajo(docenteEntidad);
	        logger.debug("Docente a almacenar "+docenteEntidad.getNombre()+" con correo "+docenteEntidad.getCorreo());
	        miembroRepository.save(docenteEntidad);
	        logger.info("Se guardo un docente en la Base de Datos");
	    }
	}
	
	/**
	 * Modifica Miembro tanto para Alumno como Docente
	 */
	@Override
	public void modificarMiembro(MiembroDto miembro) throws ModelException {
		validarExistenciaMiembroPorId(miembro);
	    validarExistenciaMiembroModificar(miembro);
	    if (miembro instanceof AlumnoDto) {
	    	Alumno alumnoEntidad =  miembroMapper.convertirAAlumno(miembro);
	    	logger.debug("Alumno a modificar "+alumnoEntidad.getNombre()+" con correo "+alumnoEntidad.getCorreo());
	        alumnoEntidad.setNombre(miembro.getNombre());
	        alumnoEntidad.setCorreo(miembro.getCorreo());
	        alumnoEntidad.setLu(((AlumnoDto) miembro).getLu());
	        alumnoEntidad.setNumeroMiembro(miembro.getNumeroMiembro());
	        miembroRepository.save(alumnoEntidad);
	        logger.info("Se modifico un alumno en la Base de Datos");
	    } else  {
	    	Docente docenteEntidad = miembroMapper.convertirADocente(miembro);
	        logger.debug("Docente a modificar "+docenteEntidad.getNombre()+" con correo "+docenteEntidad.getCorreo());
	    	docenteEntidad.setNombre(miembro.getNombre());
	        docenteEntidad.setCorreo(miembro.getCorreo());
	        docenteEntidad.setLegajo(((DocenteDto) miembro).getLegajo());
	        docenteEntidad.setNumeroMiembro(miembro.getNumeroMiembro());
	       miembroRepository.save(docenteEntidad);
	        logger.info("Se modifico un docente en la Base de Datos");
	    }
	}
	private MiembroDto validarExistenciaMiembroPorId(MiembroDto miembro) throws ModelException {
		logger.info("Se inicia el proceso de modificacion del miembro");
		if(!miembroRepository.existsById(miembro.getId())) {
			logger.error("El id del miembro no existe");
			throw new ModelException("El id "+miembro.getId()+" no existe");
		}
		return miembro;
	}
	/**
	 * Elimina Miembro tanto para Alumno como Docente
	 */
	@Override
	public void eliminarMiembro(MiembroDto miembro) throws ModelException {
	    validarExistenciaMiembro(miembro);
	    if (miembro instanceof AlumnoDto) {
	    	Alumno alumnoEliminar =  miembroMapper.convertirAAlumno(miembro);
	    	logger.debug("Alumno a eliminar "+alumnoEliminar.getNombre()+" con correo "+alumnoEliminar.getCorreo());
	    	miembroRepository.delete(alumnoEliminar);
	        logger.info("Se elimino un alumno en la Base de Datos");
	    } else  {
	    	Docente docenteEliminar = miembroMapper.convertirADocente(miembro);
	    	logger.debug("Docente a eliminar "+docenteEliminar.getNombre()+" con correo "+docenteEliminar.getCorreo());
	    	miembroRepository.delete(docenteEliminar);
	        logger.info("Se elimino un docente en la Base de Datos");
	    }
	}
	@Override
	public MiembroDto validarRegistroMiembro(MiembroDto miembro) throws ModelException{
		logger.info("Validando la existencia del miembro en la Base de Datos");
		if(miembroRepository.findByCorreo(miembro.getCorreo())!=null ||  miembroRepository.findByNumeroMiembro(miembro.getNumeroMiembro())!=null) {
			logger.error("Miembro ya registrado en la Base de Datos");
			throw new ModelException("No se puede registrar un correo o un numero de miembro que ya está en la Base de Datos");
		}
		return miembro;
	}
	@Override
	public MiembroDto validarExistenciaMiembro(MiembroDto miembro) throws ModelException {
		logger.info("Validando la existencia del miembro en la Base de Datos");
		if(miembroRepository.findByCorreo(miembro.getCorreo())==null || miembroRepository.findByNumeroMiembro(miembro.getNumeroMiembro())==null) {
			logger.error("Miembro no registrado en la Base de Datos");
			throw new ModelException("No se puede modificar o eliminar un miembro que no está en la Base de Datos");
		}
		return miembro;
	}
	private Alumno validarLU(Alumno alumnoEntidad) throws ModelException {
		if(miembroRepository.findByLu(alumnoEntidad.getLu()) !=null){
			logger.error("No se puede registrar un alumno con LU repetido");
			throw new ModelException("No se puede registrar un alumno con LU repetido");
		}
		return alumnoEntidad;
	}
	
	private Docente validarLegajo(Docente docenteEntidad) throws ModelException {
		if(miembroRepository.findByLegajo(docenteEntidad.getLegajo())!=null) {
			logger.error("No se puede registrar un docente con legajo repetido");
			throw new ModelException("No se puede registrar un docente con legajo repetido");	
		}
		return docenteEntidad;
	}
	@Override
	public MiembroDto buscarMiembroPorEmail(String email) throws ModelException {
		Miembro miembro = miembroRepository.findByCorreo(email);
		if(miembro == null) {
			logger.error("No se encontro al Miembro");
			throw new ModelException("No se puede buscar un miembro que no está en la Base de Datos");
		}

	    if( miembro instanceof Alumno) {
	    	return miembroMapper.convertirAAlumnoDto(miembro);
	    }else {
	    	return miembroMapper.convertirADocenteDto(miembro);
	    }
	}
	@Override
	public MiembroDto buscarMiembroPorNumeroMiembro(Integer numeroMiembro) throws ModelException {
		Miembro miembro = miembroRepository.findByNumeroMiembro(numeroMiembro);
		if(miembro == null) {
			logger.error("No se encontro al Miembro");
			throw new ModelException("No se puede buscar un miembro que no está en la Base de Datos");
		}
		if( miembro instanceof Alumno) {
	    	return miembroMapper.convertirAAlumnoDto(miembro);
	    }else {
	    	return miembroMapper.convertirADocenteDto(miembro);
	    }
	}
	@Override
	public Boolean validarMiembro(MiembroDto miembro) {
		if(miembroRepository.findByCorreo(miembro.getCorreo())==null && miembroRepository.findByNumeroMiembro(miembro.getNumeroMiembro())==null) {
			return false;
		}else {
			return true;
		}
	}

	@Override
	public MiembroDto buscarMiembroPorId(Integer id) throws ModelException {
		var miembro =  miembroRepository.findById(id).orElseThrow(()-> new ModelException("Miembro no encontrado"));
		return miembroMapper.convertirADocenteDto(miembro);
	}
	@Override
	public MiembroDto validarExistenciaMiembroModificar(MiembroDto miembro) throws ModelException {
		logger.info("Validando la existencia del miembro en la Base de Datos");
		if(miembroRepository.findByCorreo(miembro.getCorreo())==null && miembroRepository.findByNumeroMiembro(miembro.getNumeroMiembro())==null) {
			logger.error("Miembro no registrado en la Base de Datos");
			throw new ModelException("No se puede modificar o eliminar un miembro que no está en la Base de Datos");
		}
		return miembro;
	}
}
