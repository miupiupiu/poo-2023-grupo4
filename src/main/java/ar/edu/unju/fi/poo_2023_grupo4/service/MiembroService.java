package ar.edu.unju.fi.poo_2023_grupo4.service;

import ar.edu.unju.fi.poo_2023_grupo4.dto.MiembroDto;
import ar.edu.unju.fi.poo_2023_grupo4.exception.ModelException;

public interface MiembroService {
	public void crearMiembro(MiembroDto miembro) throws ModelException;
	public void modificarMiembro(MiembroDto miembro) throws ModelException;
	public void eliminarMiembro(MiembroDto miembro) throws ModelException;
	public MiembroDto validarRegistroMiembro(MiembroDto miembro) throws ModelException;
	public MiembroDto validarExistenciaMiembroModificar(MiembroDto miembro) throws ModelException;
	public MiembroDto validarExistenciaMiembro(MiembroDto miembro) throws ModelException;
	public MiembroDto buscarMiembroPorEmail(String email) throws ModelException;
	public MiembroDto buscarMiembroPorNumeroMiembro(Integer numeroMiembro)throws ModelException;	
	public Boolean validarMiembro(MiembroDto miembro);
	public MiembroDto buscarMiembroPorId(Integer id) throws ModelException;
}