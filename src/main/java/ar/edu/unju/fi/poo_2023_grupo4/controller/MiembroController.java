package ar.edu.unju.fi.poo_2023_grupo4.controller;

import ar.edu.unju.fi.poo_2023_grupo4.dto.AlumnoDto;
import ar.edu.unju.fi.poo_2023_grupo4.dto.DocenteDto;
import ar.edu.unju.fi.poo_2023_grupo4.exception.ModelException;
import ar.edu.unju.fi.poo_2023_grupo4.service.MiembroService;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/miembro")
public class MiembroController {
    @Autowired
    MiembroService miembroService;

    @PostMapping("/docente")
    public ResponseEntity<Map<String,String>> registrarMiembro(@RequestBody DocenteDto docenteDto) throws ModelException {
       Map<String,String> respuesta = new HashMap<>();
       try {
    	   miembroService.crearMiembro(docenteDto);
    	   respuesta.put("Mensaje: ","Docente creado con exito");
       }catch(ModelException e) {
    	   respuesta.put("Mensaje: ", "Error al crear el docente");
    	   respuesta.put("Razón: ", e.getMessage());
    	   return new ResponseEntity<>(respuesta,HttpStatus.INTERNAL_SERVER_ERROR);
       }
       return new ResponseEntity<>(respuesta,HttpStatus.CREATED);
    }

    @PostMapping("/alumno")
    public ResponseEntity<Map<String,String>> registrarMiembro(@RequestBody AlumnoDto alumnoDto) throws ModelException {
        Map<String,String> respuesta = new HashMap<>();
        try {
        	miembroService.crearMiembro(alumnoDto);
     	   respuesta.put("Mensaje: ","Alumno creado con exito");
        }catch(ModelException e) {
      	   respuesta.put("Mensaje: ", "Error al crear el alumno");
      	   respuesta.put("Razón: ", e.getMessage());
    	   return new ResponseEntity<>(respuesta,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(respuesta,HttpStatus.CREATED);
    }

}
