package ar.edu.unju.fi.poo_2023_grupo4.controller;

import java.time.LocalDate;

import ar.edu.unju.fi.poo_2023_grupo4.dto.DevolucionDto;
import ar.edu.unju.fi.poo_2023_grupo4.dto.EndPointsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.itextpdf.io.exceptions.IOException;
import java.io.ByteArrayOutputStream;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import ar.edu.unju.fi.poo_2023_grupo4.dto.PrestamoDto;
import ar.edu.unju.fi.poo_2023_grupo4.exception.ModelException;
import ar.edu.unju.fi.poo_2023_grupo4.service.PrestamoService;
import ar.edu.unju.fi.poo_2023_grupo4.strategy.PrestamoGeneradorResumenStrategy;
import ar.edu.unju.fi.poo_2023_grupo4.strategy.imp.PrestamoGeneradorResumenExcelStrategyImp;
import ar.edu.unju.fi.poo_2023_grupo4.strategy.imp.PrestamoGeneradorResumenPDFStrategyImp;
import jakarta.mail.MessagingException;

@RestController
@RequestMapping("/prestamo")
public class PrestamoController {

    @Autowired
    PrestamoService prestamoService;

    @PostMapping
    public ResponseEntity<?> crearPrestamo(@RequestBody EndPointsDto endPointsDto){
        try {
            PrestamoDto prestamoCreado = prestamoService.crearPrestamo(endPointsDto);
            return ResponseEntity.ok(prestamoCreado);
        } catch (ModelException | MessagingException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping("/devolucion/{id}")
    public ResponseEntity<?> devolverPrestamo(@PathVariable("id") int id){
        try {
            DevolucionDto prestamoDevuelto = prestamoService.devolverPrestamo(id);

            if (prestamoDevuelto != null) {
                return ResponseEntity.ok(prestamoDevuelto);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        } catch (ModelException | MessagingException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }

    }
    
    @GetMapping("/resumen")
    public ResponseEntity<?> obtenerResumen(@RequestParam String desde, @RequestParam String hasta, @RequestParam String formato) throws ModelException, IOException, java.io.IOException {
    	String tipoArchivo = null;
    	PrestamoGeneradorResumenStrategy estrategia;
        if ("pdf".equalsIgnoreCase(formato)) {
        	tipoArchivo=".pdf";
            estrategia = new PrestamoGeneradorResumenPDFStrategyImp();
        } else if ("excel".equalsIgnoreCase(formato)) {
        	tipoArchivo=".xls";
            estrategia = new PrestamoGeneradorResumenExcelStrategyImp();
        } else {
            return ResponseEntity.badRequest().body(null);
        }
        try {
            prestamoService.setResumenStrategy(estrategia);
            ByteArrayOutputStream byteArrayOutputStream = prestamoService.crearResumenPrestamos(desde, hasta);
            if(byteArrayOutputStream == null) {
            	return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
            }
            byte[] fileBytes = byteArrayOutputStream.toByteArray();
            
            MediaType mediaType;
            if ("pdf".equalsIgnoreCase(formato)) {
                mediaType = MediaType.APPLICATION_PDF;
            } else if ("excel".equalsIgnoreCase(formato)) {
                mediaType = MediaType.APPLICATION_OCTET_STREAM;
            } else {
                return ResponseEntity.badRequest().body(null);
            }

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(mediaType);
            headers.setContentLength(fileBytes.length);
            headers.setContentDispositionFormData("attachment",LocalDate.now().toString()+"-prestamos"+tipoArchivo);
            ByteArrayResource resource = new ByteArrayResource(fileBytes);
            return ResponseEntity.ok().headers(headers).body(resource);
        }catch(ModelException | IOException | java.io.IOException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
