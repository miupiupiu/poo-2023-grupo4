package ar.edu.unju.fi.poo_2023_grupo4.service;

import ar.edu.unju.fi.poo_2023_grupo4.dto.LibroDto;
import ar.edu.unju.fi.poo_2023_grupo4.exception.ModelException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class LibroTest {

    @Autowired
    LibroService libroService;

    static LibroDto libro1;
    static LibroDto libro2;
    static LibroDto libro3;

    @BeforeAll
    static void setUp(){

        libro1 = new LibroDto();
        libro1.setAutor("Nana");
        libro1.setIsbn("978-1-56619-909-5");
        libro1.setTitulo("El arte de la cocina Italiana");
        libro1.setNumInventario(105201);
        libro1.setEstadoLibro("BAJA");

        libro2 = new LibroDto();
        libro2.setIsbn("978-1-56619-909-0");
        libro2.setAutor("Nana");
        libro2.setTitulo("Recetas con ciruelas");
        libro2.setNumInventario(501002);
        libro2.setEstadoLibro("BAJA");

        libro3 = new LibroDto();
        libro3.setIsbn("978-1-56619-909-0");
        libro3.setAutor("Nana");
        libro3.setTitulo("Recetas con ciruelas");
        libro3.setNumInventario(501003);
        libro3.setEstadoLibro("BAJA");
    }

    @AfterAll
    static void tearDown(){
        libro1 = null;
        libro2 = null;
        libro3 = null;
    }

    @Test
    void crearLibroConIsbnInexistente() throws ModelException {
        LibroDto libroCreado = libroService.crearLibro(libro1);
        assertEquals(libro1.getIsbn(), libroCreado.getIsbn());
        assertEquals(libro1.getAutor(), libroCreado.getAutor());
    }

    @Test
    void crearLibroConIsbnYNumeroDeInventarioRepetido(){
        assertThrows(ModelException.class, ()->libroService.crearLibro(libro2));
    }

    @Test
    void crearLibroConIsbnExistenteYNumeroDeInventarioNuevo() throws ModelException {
        LibroDto libroCreado = libroService.crearLibro(libro3);
        assertEquals(libro3.getIsbn(), libroCreado.getIsbn());
        assertEquals(libro3.getNumInventario(), libroCreado.getNumInventario());
    }

    @Test
    void eliminarLibroExistentePorId() throws  ModelException {
        libroService.eliminarLibro(8);
        assertFalse(libroService.verificarExistenciaPorId(8));

    }

    @Test
    void eliminarLibroInexistentePorId(){
        assertThrows(ModelException.class, ()->libroService.eliminarLibro(100));
    }

    @Test
    void modificarLibroExistenteSinCambiarNroDeInventario() throws ModelException {
        var libroModificado = new LibroDto();
        libroModificado.setId(2);
        libroModificado.setIsbn("978-1-56619-909-3");
        libroModificado.setAutor("Agata");
        libroModificado.setTitulo("Recuerdos de primavera");
        libroModificado.setNumInventario(102117);
        libroModificado.setEstadoLibro("BAJA");
        var resultado = libroService.modificarLibro(libroModificado);
        assertEquals(libroModificado.getAutor(), resultado.getAutor());
    }

    @Test
    void modificarLibroInexistente() throws ModelException {
        var libroModificado = new LibroDto();
        libroModificado.setId(100);
        libroModificado.setIsbn("978-1-56619-909-3");
        libroModificado.setAutor("Agata");
        libroModificado.setTitulo("Recuerdos de primavera");
        libroModificado.setNumInventario(102117);
        libroModificado.setEstadoLibro("BAJA");
        assertThrows(ModelException.class, ()->libroService.crearLibro(libroModificado));
    }

    @Test
    void modificarLibroExistenteConNroInventarioDuplicado() throws ModelException {
        var libroModificado = new LibroDto();
        libroModificado.setId(2);
        libroModificado.setIsbn("978-1-56619-909-3");
        libroModificado.setAutor("Agata");
        libroModificado.setTitulo("Recuerdos de primavera");
        libroModificado.setNumInventario(200301);
        libroModificado.setEstadoLibro("BAJA");
        assertThrows(ModelException.class, ()->libroService.crearLibro(libroModificado));
    }

    @Test
    void buscarLibroPorAutorExistente(){
        var resultados = libroService.buscarLibrosPorAutor("Desconocido");
        assertEquals(resultados.size(), 1);
    }

    @Test
    void buscarLibroPorAutorInexistente(){
        var resultados = libroService.buscarLibrosPorAutor("Khronos");
        assertEquals(resultados.size(), 0);
    }

    @Test
    void buscarLibroPorTituloExistente(){
        var resultados = libroService.buscarLibrosPorTitulo("Recuerdos de primavera");
        assertEquals(resultados.size(), 1);
    }

    @Test
    void buscarLibroPorTituloInexistente(){
        var resultados = libroService.buscarLibrosPorTitulo("Como comer papas al vapor");
        assertEquals(resultados.size(), 0);
    }

    @Test
    void buscarLibroPorIsbnExistente(){
        var resultados = libroService.buscarLibrosPorIsbn("978-1-56619-909-2");
        assertEquals(resultados.size(), 1);
    }

    @Test
    void buscarLibroPorIsbnInexistente(){
        var resultados = libroService.buscarLibrosPorIsbn("978-1-56619-909-100");
        assertEquals(resultados.size(), 0);
    }

}
