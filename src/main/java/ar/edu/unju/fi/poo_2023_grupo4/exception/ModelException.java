package ar.edu.unju.fi.poo_2023_grupo4.exception;

public class ModelException extends Exception{
	private static final long serialVersionUID = 1L;
	public ModelException(String message){
	        super(message);
	   }
}
