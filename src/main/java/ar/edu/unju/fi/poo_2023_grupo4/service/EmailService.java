package ar.edu.unju.fi.poo_2023_grupo4.service;

import ar.edu.unju.fi.poo_2023_grupo4.dto.EmailDto;
import jakarta.mail.MessagingException;

public interface EmailService {
    public String sendHtmlEmail(EmailDto email) throws MessagingException;
}
