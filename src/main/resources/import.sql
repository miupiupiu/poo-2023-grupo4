-- CREAR LIBROS
insert into libro (num_inventario, autor, estado_libro, isbn, titulo) values (102025, "Nana", "BAJA" ,"978-1-56619-909-4", "Como comer maiz en 3 simples pasos");
insert into libro (num_inventario, autor, estado_libro, isbn, titulo) values (102117, "Pandy","DISPONIBLE" ,"978-1-56619-909-3", "Recuerdos de primavera");
insert into libro (num_inventario, autor, estado_libro, isbn, titulo) values (200301, "Nana","PRESTADO" ,"978-1-56619-909-2", "Plantas de estacion");
insert into libro (num_inventario, autor, estado_libro, isbn, titulo) values (300501, "Desconocido","DISPONIBLE" ,"978-1-56619-909-1", "Mis memorias");
insert into libro (num_inventario, autor, estado_libro, isbn, titulo) values (501001, "Nana", "PRESTADO","978-1-56619-909-0", "Recetas con ciruelas");
insert into libro (num_inventario, autor, estado_libro, isbn, titulo) values (501002, "Nana", "PRESTADO","978-1-56619-909-0", "Recetas con ciruelas");
insert into libro (num_inventario, autor, estado_libro, isbn, titulo) values (401001, "Dudu", "DISPONIBLE","978-1-56619-909-7", "Comer croquetas de pescado es sano");
insert into libro (num_inventario, autor, estado_libro, isbn, titulo) values (401002, "Dudu", "DISPONIBLE","978-1-56619-909-7", "Comer croquetas de pescado es sano");
insert into libro (num_inventario, autor, estado_libro, isbn, titulo) values (601002, "Aghata", "PRESTADO","978-1-56619-909-8", "Caer con estilo");

-- CREAR MIEMBROS
INSERT INTO miembros(numero_miembro, tipo_miembro, correo, legajo, lu, nombre, telefono) VALUES(1, 'Docente', 'josemanuelfernanddez@gmail.com', 'AA001', NULL, 'Jose Manuel Fernandez', '3885084220');
INSERT INTO miembros(numero_miembro, tipo_miembro, correo, legajo, lu, nombre, telefono) VALUES(2, 'Docente', 'roaguerrero@gmail.com', 'AB002', NULL, 'Sofia', '116523225');
INSERT INTO miembros(numero_miembro, tipo_miembro, correo, legajo, lu, nombre, telefono) VALUES(3, 'Alumno', 'pauldoe@email.com', NULL, 'FI001', 'Paul Doe', '116523212');
INSERT INTO miembros(numero_miembro, tipo_miembro, correo, legajo, lu, nombre, telefono) VALUES(4, 'Alumno', 'miupiupiu@gmail.com', NULL, 'FI002', 'Miu', '116523213');
INSERT INTO miembros(numero_miembro, tipo_miembro, correo, legajo, lu, nombre, telefono) VALUES(5, 'Docente', 'jarana@gmail.com', 'JI001', 'NULL', 'Jolin Cyrus', '3884968014');
INSERT INTO miembros(numero_miembro, tipo_miembro, correo, legajo, lu, nombre, telefono) VALUES(6, 'Docente', 'carolina@gmail.com', 'CI001', 'NULL', 'Constanza Carolina', '987456123');
INSERT INTO miembros(numero_miembro, tipo_miembro, correo, legajo, lu, nombre, telefono) VALUES(7, 'Alumno', 'juariu@gmail.com', 'NULL', 'JA001', 'Janice Sour', '7896584231');
INSERT INTO miembros(numero_miembro, tipo_miembro, correo, legajo, lu, nombre, telefono,fecha_hasta_sancion) VALUES(8, 'Docente', 'roaguerrero17@email.com', 'AB002', NULL, 'Angeles', '116523225','2023-12-05 16:00:00');

-- CREAR PRESTAMOS
insert into prestamo (libro_id, miembro_id, fecha_devolucion, fecha_prestamo, estado_prestamo) values (1, 1, '2023-10-15 15:00:00', '2023-10-05 15:00:00',"PRESTADO")
insert into prestamo (libro_id, miembro_id, fecha_devolucion, fecha_prestamo, estado_prestamo) values (2, 2, '2023-10-16 15:00:00', '2023-10-06 15:00:00',"PRESTADO")
insert into prestamo (libro_id, miembro_id, fecha_devolucion, fecha_prestamo, estado_prestamo) values (5, 7, '2023-11-06 15:00:00', '2023-11-10 19:00:00',"PRESTADO")