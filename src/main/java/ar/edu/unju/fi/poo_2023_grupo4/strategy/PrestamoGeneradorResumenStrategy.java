package ar.edu.unju.fi.poo_2023_grupo4.strategy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import ar.edu.unju.fi.poo_2023_grupo4.dto.PrestamoDto;

public interface PrestamoGeneradorResumenStrategy {
	ByteArrayOutputStream generarResumen(List<PrestamoDto> prestamos) throws IOException;
}