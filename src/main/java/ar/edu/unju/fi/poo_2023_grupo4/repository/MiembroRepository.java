package ar.edu.unju.fi.poo_2023_grupo4.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.poo_2023_grupo4.entity.Alumno;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Docente;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Miembro;

@Repository
public interface MiembroRepository extends CrudRepository<Miembro,Integer> {
	Boolean existsByCorreo(String correo);
	Boolean existsByNumeroMiembro(int numeroMiembro);
	Miembro findByCorreo(String correo);
	Miembro findByNumeroMiembro(Integer numeroMiembro);
	Alumno findByLu(String lu);
	Docente findByLegajo(String legajo);
}
