package ar.edu.unju.fi.poo_2023_grupo4.service;

import ar.edu.unju.fi.poo_2023_grupo4.dto.*;
import ar.edu.unju.fi.poo_2023_grupo4.exception.ModelException;
import jakarta.mail.MessagingException;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class PrestamoTest {

    @Autowired
    PrestamoService prestamoService;
    @Autowired
    MiembroService miembroService;
    @Autowired
    LibroService libroService;

    static PrestamoDto prestamo;
    static EndPointsDto endPointsDto;
    static LibroDto libro;
    static DocenteDto docente;
    static AlumnoDto alumno;
    static  MiembroDto miembro;
    static  DevolucionDto devolucionDto;

    @BeforeAll
    static void setUp() {
        prestamo = new PrestamoDto();
        prestamo.setEstadoPrestamo("prestado");
        prestamo.setFechaPrestamo("2023-10-01 15:00");
        prestamo.setFechaDevolucion("2023-10-15 15:00");
        docente = new DocenteDto();
        miembro = new MiembroDto();
        endPointsDto = new EndPointsDto();
        devolucionDto = new DevolucionDto();

    }

    @AfterAll
    static void tearDown() {
        prestamo = null;
        docente = null;
        alumno = null;
        libro = null;
        miembro = null;
        endPointsDto = null;
        devolucionDto = null;
    }

    @Test
    void crearPrestamoConDocente() throws ModelException, MessagingException {
        miembro= miembroService.buscarMiembroPorEmail("roaguerrero@gmail.com");
        endPointsDto.setMiembroId(miembro.getId());
        libro = libroService.obtenerLibroPorId(2);
        endPointsDto.setLibroId(libro.getId());
        PrestamoDto prestamoDto = prestamoService.crearPrestamo(endPointsDto);
        assertEquals("PRESTADO", prestamoDto.getEstadoPrestamo());
    }

    @Test
    void crearPrestamoConAlumno() throws ModelException, MessagingException {
        miembro= miembroService.buscarMiembroPorEmail("miupiupiu@gmail.com");
        endPointsDto.setMiembroId(miembro.getId());
        libro = libroService.obtenerLibroPorId(4);
        endPointsDto.setLibroId(libro.getId());
        PrestamoDto prestamoDto = prestamoService.crearPrestamo(endPointsDto);
        assertEquals("PRESTADO", prestamoDto.getEstadoPrestamo());
    }

    @Test
    void crearPrestamoConLibroDadoDeBaja() throws ModelException {
        libro = libroService.obtenerLibroPorId(1);
        endPointsDto.setLibroId(libro.getId());
        assertThrows(ModelException.class, () -> prestamoService.crearPrestamo(endPointsDto));
    }

    @Test
    void crearPrestamoConLibroPrestado() throws ModelException {
        libro = libroService.obtenerLibroPorId(3);
        endPointsDto.setLibroId(libro.getId());
        assertThrows(ModelException.class, () -> prestamoService.crearPrestamo(endPointsDto));
    }

    @Test
    void devolverPrestamo() throws ModelException, MessagingException {
    	var devolucion = prestamoService.devolverPrestamo(3);
    	assertEquals("DEVUELTO",devolucion.getEstadoPrestamo());
    }


}
