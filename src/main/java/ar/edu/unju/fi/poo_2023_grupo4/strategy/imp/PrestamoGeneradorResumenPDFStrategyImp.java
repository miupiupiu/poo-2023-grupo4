package ar.edu.unju.fi.poo_2023_grupo4.strategy.imp;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;

import ar.edu.unju.fi.poo_2023_grupo4.dto.PrestamoDto;
import ar.edu.unju.fi.poo_2023_grupo4.strategy.PrestamoGeneradorResumenStrategy;


public class PrestamoGeneradorResumenPDFStrategyImp implements PrestamoGeneradorResumenStrategy {

	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
	
	@Override
	public ByteArrayOutputStream generarResumen(List<PrestamoDto> prestamos) throws FileNotFoundException, MalformedURLException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		PdfWriter writer = new PdfWriter(byteArrayOutputStream);
		PdfDocument pdf = new PdfDocument(writer);
		Document document = new Document(pdf);
		Image image = new Image(ImageDataFactory.create(new URL("https://i.imgur.com/A01q5fT.png")))
				.setWidth(100)
				.setHeight(100)
				.setHorizontalAlignment(HorizontalAlignment.CENTER);
		agregarEncabezado(document);
		agregarCuerpo(document,prestamos);
		document.add(image);
		document.close();
		return byteArrayOutputStream;
	}

	private void agregarEncabezado(Document document) {
		Paragraph titulo = new Paragraph("Resumen de Préstamos de la Biblioteca");
		titulo.setFontSize(20);
		titulo.setBold();
		titulo.setTextAlignment(TextAlignment.CENTER);
		titulo.setMarginBottom(20);
		
		Paragraph fechaHora = new Paragraph("Fecha y hora: "+LocalDateTime.now().format(formatter));
		fechaHora.setFontSize(10);
		fechaHora.setTextAlignment(TextAlignment.LEFT);
		
		document.add(titulo);
		document.add(fechaHora);
		document.add(new Paragraph("\n"));
	}

	private void agregarCuerpo(Document document, List<PrestamoDto> prestamos) {
        Table table = new Table(UnitValue.createPercentArray(8))
                .useAllAvailableWidth()
                .setMarginBottom(20);
		
        Color encabezadoFondoColor = new DeviceRgb(3, 252, 194);

        String[] columnas = {"N°", "FECHA Y HORA DE PRÉSTAMO", "FECHA Y HORA DE DEVOLUCIÓN", "FECHA Y HORA DE DEVOLUCIÓN POR MIEMBRO", "NÚM. INV. E ISBN DEL LIBRO", "NÚM. DEL MIEMBRO", "ESTADO", "SANCION"};
        
        for (String columna : columnas) {
        	table.addHeaderCell(crearCelda(columna, true, encabezadoFondoColor));
        }

        int siguienteNro = 1;
        for (PrestamoDto p : prestamos) {
            table.addCell(crearCelda(siguienteNro++, false, null));
            table.addCell(crearCelda(p.getFechaPrestamo(), false, null));
            table.addCell(crearCelda(p.getFechaDevolucion(), false, null));
            table.addCell(crearCelda(p.getFechaDevolucionPorMiembro() != null ? p.getFechaDevolucionPorMiembro() : "Aun sin entregar" , false, null));
            table.addCell(crearCelda(p.getLibro().getNumInventario() + "-" + p.getLibro().getIsbn(), false, null));
            table.addCell(crearCelda(p.getMiembro().getNumeroMiembro(),false,null)); 
            table.addCell(crearCelda(p.getEstadoPrestamo(), false, null));
            table.addCell(crearCelda(p.getMiembro().getFechaHastaSancion() != null ? p.getMiembro().getFechaHastaSancion().format(formatter) : "Sin Sanción", false, null));
            }

        document.add(table);
    }

    private Cell crearCelda(Object objeto, boolean esEncabezado, Color fondoColor) {
        Cell celda = new Cell().add(new Paragraph(String.valueOf(objeto)))
                .setFontSize(7)
                .setTextAlignment(TextAlignment.CENTER);
        if (esEncabezado) {
            if (fondoColor != null) {
            	celda.setBold();
            	celda.setItalic();
                celda.setBackgroundColor(fondoColor);
            }
        }
        return celda;
    }
}