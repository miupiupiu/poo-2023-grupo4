package ar.edu.unju.fi.poo_2023_grupo4.util;

import ar.edu.unju.fi.poo_2023_grupo4.entity.Prestamo;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;

@Component
public class Recibo {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    public static String html(Prestamo prestamo) {
        return 
                "<html>\n" +
                "<head>\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "    <style>\n" +
                "        body {\n" +
                "            background-color: #FFF8E7; /* Amarillo pálido */\n" +
                "            font-family: 'Arial', sans-serif;\n" +
                "            color: #1E429B; /* Azul */\n" +
                "        }\n" +
                "        .container {\n" +
                "            background: #FFFFFF; /* Blanco */\n" +
                "            border: 2px solid #FFD700; /* Dorado */\n" +
                "            padding: 20px;\n" +
                "            margin: 20px;\n" +
                "            text-align: center;\n" +
                "        }\n" +
                "        .container img {\n" +
                "            max-width: 80%;\n" +
                "        }\n" +
                "        h1 {\n" +
                "            color: #FFD700; /* Dorado */\n" +
                "            font-family: 'Arial', sans-serif;\n" +
                "            font-size: 28px;\n" +
                "            text-transform: uppercase;\n" +
                "        }\n" +
        "        .saludos {\n" +
                "            font-size: 18px;\n" +
                "            font-weight: bold;\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <div class=\"container\">\n" +
                "		 <img src='https://i.imgur.com/kehcw9U.png' alt='Gato con libro' />\n"+
                "        <h1>BIBLIOTECATS - Recibo de préstamo</h1>\n" +
                "        <div class='saludos'>\n" +
                "             Señor/a: "+prestamo.getMiembro().getNombre() +"</div>\n" +
                "		  <p> Usted tiene un prestamo por el siguiente libro \""+prestamo.getLibro().getTitulo()+"\"</p>\n"+
                "		  <p> Fecha de prestamo "+prestamo.getFechaPrestamo().format(formatter)+"</p> \n"+
                "		  <p> Fecha de devolucion "+prestamo.getFechaDevolucion().format(formatter)+"</p> \n"+
                "        <div class='saludos'>\n" +
                "            ¡Gracias por elegir Bibliotecats para tus aventuras literarias felinas!</div>\n" +
                "        <p>Esperamos que disfrutes tu libro y que tu gato te acompañe en esta lectura.</p>\n" +
                "        <p>¡Saludos gatunos y que los libros te inspiren!</p>\n" +
        		"	     <img src='https://i.imgur.com/A01q5fT.png' width='10%'>\n"
                +"    </div>\n" +
                "</body>\n" +
                "</html>";
    }
}
