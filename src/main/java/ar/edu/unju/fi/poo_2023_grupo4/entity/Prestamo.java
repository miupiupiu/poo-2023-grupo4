package ar.edu.unju.fi.poo_2023_grupo4.entity;

import ar.edu.unju.fi.poo_2023_grupo4.util.EstadoPrestamo;
import jakarta.persistence.*;

import java.time.LocalDateTime;

@Entity
public class Prestamo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "miembro_id")
    private Miembro miembro;
    @ManyToOne
    @JoinColumn(name = "libro_id")
    private Libro libro;
    @Column(name = "fecha_prestamo")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime fechaPrestamo;
    @Column(name = "fecha_devolucion")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime fechaDevolucion;
    @Column(name = "fecha_devolucion_miembro")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime fechaDevolucionPorMiembro;
    @Enumerated(EnumType.STRING)
    @Column(name = "estado_prestamo")
    private EstadoPrestamo estadoPrestamo;
    @Column(name = "pdf_url")
    private String pdfUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Miembro getMiembro() {
        return miembro;
    }

    public void setMiembro(Miembro miembro) {
        this.miembro = miembro;
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    public LocalDateTime getFechaPrestamo() {
        return fechaPrestamo;
    }

    public void setFechaPrestamo(LocalDateTime fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }

    public LocalDateTime getFechaDevolucion() {
        return fechaDevolucion;
    }

    public void setFechaDevolucion(LocalDateTime fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    public LocalDateTime getFechaDevolucionPorMiembro() {
		return fechaDevolucionPorMiembro;
	}

	public void setFechaDevolucionPorMiembro(LocalDateTime fechaDevolucionPorMiembro) {
		this.fechaDevolucionPorMiembro = fechaDevolucionPorMiembro;
	}

	public EstadoPrestamo getEstadoPrestamo() {
        return estadoPrestamo;
    }

    public void setEstadoPrestamo(EstadoPrestamo estadoPrestamo) {
        this.estadoPrestamo = estadoPrestamo;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }
}
