package ar.edu.unju.fi.poo_2023_grupo4.service.imp;

import ar.edu.unju.fi.poo_2023_grupo4.dto.LibroDto;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Libro;
import ar.edu.unju.fi.poo_2023_grupo4.exception.ModelException;
import ar.edu.unju.fi.poo_2023_grupo4.repository.LibroRepository;
import ar.edu.unju.fi.poo_2023_grupo4.service.LibroService;
import ar.edu.unju.fi.poo_2023_grupo4.util.LibroMapper;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LibroServiceImp implements LibroService {

	private LibroRepository libroRepository;

    private LibroMapper libroMapper;
    
	private static final Logger logger = Logger.getLogger(LibroServiceImp.class);    
    
    @Autowired
    public LibroServiceImp(LibroRepository libroRepository, LibroMapper libroMapper) {
        this.libroRepository = libroRepository;
        this.libroMapper = libroMapper;
    }
    
    /**
     * Este metodo crea un libro a partir del envio de un objeto de tipo LibroDto
     */
    @Override
    public LibroDto crearLibro(LibroDto libroDto) throws ModelException {
    	logger.info("Iniciando la creacion del Libro");
        Libro libro = libroMapper.convertirALibro(libroDto);
        if (verificarExistenciaPorIsbn(libro.getIsbn())) {
            if (verificarExistenciaPorNumInventario(libro.getNumInventario())) {
            	logger.error("Ya existe el libro con este numero de inventario");
                throw new ModelException("El numero de inventario : " + libro.getNumInventario() + " ya existe");
            } else {
            	logger.debug("Se guardara el libro con el siguiente titulo "+libro.getTitulo());
                libro = libroRepository.save(libro);
                logger.info("Se guardo el libro en la Base de Datos");
            }
        } else {
        	logger.debug("Se guardara el libro con el siguiente titulo "+libro.getTitulo());
            libro = libroRepository.save(libro);
            logger.info("Se guardo el libro en la Base de Datos");
        }
        return libroMapper.convertirALibroDTO(libro);
    }

    @Override
    public boolean verificarExistenciaPorIsbn(String isbn) {
    	logger.info("Se inicia la verificacion de la existencia por el ISBN");
        return libroRepository.existsByIsbn(isbn);
    }

    @Override
    public boolean verificarExistenciaPorNumInventario(Integer numInventario) {
    	logger.info("Se inicia la verificacion de la existencia por el Numero de Inventario");
        return libroRepository.existsByNumInventario(numInventario);
    }

    @Override
    public void eliminarLibro(Integer id) throws ModelException {
    	logger.info("Se inicia la eliminacion del libro");
        if (libroRepository.existsById(id)) {
        	logger.debug("Se eliminara el Libro con el siguiente id "+id);
            libroRepository.deleteById(id);
        }else{
        	logger.error("El id no existe");
            throw new ModelException("El id: " + id + " no existe");
        }
    }

    @Override
    public boolean verificarExistenciaPorId(Integer id) {
    	logger.info("Se inicia la verificacion de la existencia por el ID");
        return libroRepository.existsById(id);
    }

    @Override
    public LibroDto modificarLibro(LibroDto libroDto) throws ModelException {
    	logger.info("Se inicia el proceso de modificacion del libro");
        Libro libro = libroMapper.convertirALibro(libroDto);
        if (libroRepository.existsById(libro.getId())){
            if (libroRepository.existsByNumInventario(libro.getNumInventario())){
                LibroDto libroOriginal = obtenerLibroPorId(libroDto.getId());
                logger.debug("Se va a modificar el libro "+libroOriginal.getTitulo());
                if(libroOriginal.getNumInventario()==libro.getNumInventario()){
                    libro = libroRepository.save(libro);
                    logger.info("Se guardo el libro modificado");
                }
                else {
                	logger.error("El numero de inventario ya existe");
                    throw new ModelException("El numero de inventario : " + libro.getNumInventario() + " ya existe");
                }
            }else {
                libro = libroRepository.save(libro);
                logger.info("Se guardo el libro modificado");
            }
        }else{
        	logger.error("El id no existe");
            throw new ModelException("El id: " + libro.getId() + " no existe");
        }
        return libroMapper.convertirALibroDTO(libro);
    }

    @Override
    public LibroDto obtenerLibroPorId(Integer id) throws ModelException {
        var libro = libroRepository.findById(id).orElseThrow(()-> new ModelException("El id: " + id + " no existe"));
        return libroMapper.convertirALibroDTO(libro);
    }

    @Override
    public List<LibroDto> buscarLibrosPorAutor(String autor) {
        List<Libro> librosPorAutor = libroRepository.findByAutor(autor);
        return librosPorAutor.stream().map(libro ->libroMapper.convertirALibroDTO(libro)).collect(Collectors.toList());
    }

    @Override
    public List<LibroDto> buscarLibrosPorTitulo(String titulo) {
        List<Libro> librosPorAutor = libroRepository.findByTitulo(titulo);
        return librosPorAutor.stream().map(libro ->libroMapper.convertirALibroDTO(libro)).collect(Collectors.toList());
    }

    @Override
    public List<LibroDto> buscarLibrosPorIsbn(String isbn) {
        List<Libro> librosPorAutor = libroRepository.findByIsbn(isbn);
        return librosPorAutor.stream().map(libro ->libroMapper.convertirALibroDTO(libro)).collect(Collectors.toList());
    }
}
