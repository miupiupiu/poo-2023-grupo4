package ar.edu.unju.fi.poo_2023_grupo4.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class MiembroDto implements Serializable{
		private static final long serialVersionUID = 1L;
		private int id;
		private String nombre;
		private Integer numeroMiembro;
		private String correo;
		private LocalDateTime fechaHastaSancion;
		
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public Integer getNumeroMiembro() {
			return numeroMiembro;
		}
		public void setNumeroMiembro(Integer numeroMiembro) {
			this.numeroMiembro = numeroMiembro;
		}
		public String getCorreo() {
			return correo;
		}
		public void setCorreo(String correo) {
			this.correo = correo;
		}

	public LocalDateTime getFechaHastaSancion() {
		return fechaHastaSancion;
	}

	public void setFechaHastaSancion(LocalDateTime fechaHastaSancion) {
		this.fechaHastaSancion = fechaHastaSancion;
	}
}
