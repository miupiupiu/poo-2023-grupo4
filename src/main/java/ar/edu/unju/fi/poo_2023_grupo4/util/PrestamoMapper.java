package ar.edu.unju.fi.poo_2023_grupo4.util;

import ar.edu.unju.fi.poo_2023_grupo4.dto.PrestamoDto;
import ar.edu.unju.fi.poo_2023_grupo4.entity.Prestamo;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class PrestamoMapper {
    private final ModelMapper modelMapper;

    public PrestamoMapper() {
        this.modelMapper = new ModelMapper();
        this.modelMapper.getConfiguration().setSkipNullEnabled(true);
        this.setConverters();
    }

    private void setConverters() {
        Converter<String, EstadoPrestamo> estadoPrestamoConverter = context -> EstadoPrestamo.valueOf(context.getSource().toUpperCase());
        Converter<String, LocalDateTime> fechasConverter = context -> {
            String source = context.getSource();
            if (source == null) {
                return null;
            }
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            return LocalDateTime.parse(source, formatter);
        };
        Converter<LocalDateTime, String> fechasStringConverter = context -> {
            LocalDateTime source = context.getSource();
            if (source == null) {
                return null;
            }
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            return source.format(formatter);
        };
        Converter<String, EstadoLibro> estadoLibroConverter = context -> EstadoLibro.valueOf(context.getSource().toUpperCase());
        modelMapper.createTypeMap(PrestamoDto.class, Prestamo.class)
                .addMappings(mapping -> {
                    mapping.using(estadoPrestamoConverter).map(PrestamoDto::getEstadoPrestamo, Prestamo::setEstadoPrestamo);
                    mapping.using(fechasConverter).map(PrestamoDto::getFechaPrestamo, Prestamo::setFechaPrestamo);
                    mapping.using(fechasConverter).map(PrestamoDto::getFechaDevolucion, Prestamo::setFechaDevolucion);
                    mapping.using(fechasConverter).map(PrestamoDto::getFechaDevolucionPorMiembro, Prestamo::setFechaDevolucionPorMiembro);
                    mapping.map(PrestamoDto::getEstadoPrestamo, Prestamo::setEstadoPrestamo);
                    mapping.skip((dest, value) -> dest.getLibro().setEstadoLibro((EstadoLibro) value));
                });

        modelMapper.createTypeMap(Prestamo.class, PrestamoDto.class)
                .addMappings(mapping -> {
                    mapping.map(Prestamo::getEstadoPrestamo, PrestamoDto::setEstadoPrestamo);
                    mapping.using(fechasStringConverter).map(Prestamo::getFechaPrestamo, PrestamoDto::setFechaPrestamo);
                    mapping.using(fechasStringConverter).map(Prestamo::getFechaDevolucion, PrestamoDto::setFechaDevolucion);
                    mapping.using(fechasStringConverter).map(Prestamo::getFechaDevolucionPorMiembro, PrestamoDto::setFechaDevolucionPorMiembro);
                    mapping.map(Prestamo::getEstadoPrestamo, PrestamoDto::setEstadoPrestamo);
                });
    }

    public PrestamoDto convertirAPrestamoDto(Prestamo prestamo) {
        return modelMapper.map(prestamo, PrestamoDto.class);
    }

    public Prestamo convertirAPrestamo(PrestamoDto prestamoDto) {
        return modelMapper.map(prestamoDto, Prestamo.class);
    }
}
