package ar.edu.unju.fi.poo_2023_grupo4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Poo2023Grupo4Application {

	public static void main(String[] args) {
		SpringApplication.run(Poo2023Grupo4Application.class, args);
	}

}
