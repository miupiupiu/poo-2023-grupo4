
package ar.edu.unju.fi.poo_2023_grupo4.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.unju.fi.poo_2023_grupo4.dto.EmailDto;
import jakarta.mail.MessagingException;

@SpringBootTest
public class MailTest {
    @Autowired
    private EmailService emailService;
  
    @Test
    void enviarMail() throws MessagingException {
    	String to = "josemanuelfernanddez@gmail.com";
    	String subject = "Prueba";
    	String body = "<h1> Hola Mundo </h1>";
    	EmailDto email = new EmailDto();
    	email.setTo(to);
    	email.setSubject(subject);
    	email.setBody(body);
        emailService.sendHtmlEmail(email);
        assertEquals("Enviado",emailService.sendHtmlEmail(email));
    }
    
    @Test
    void enviarMailFormatoIncorrecto() throws MessagingException {
    	EmailDto email = new EmailDto();
    	email.setTo("josefernandez.com");
    	email.setSubject("Prueba");
    	email.setBody("<h1> Hola Mundo </h1>");
        assertEquals("No enviado, error en el formato",emailService.sendHtmlEmail(email));
    }
}
