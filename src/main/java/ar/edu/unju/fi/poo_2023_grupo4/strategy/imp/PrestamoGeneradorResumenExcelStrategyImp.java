package ar.edu.unju.fi.poo_2023_grupo4.strategy.imp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;

import ar.edu.unju.fi.poo_2023_grupo4.dto.PrestamoDto;
import ar.edu.unju.fi.poo_2023_grupo4.strategy.PrestamoGeneradorResumenStrategy;

public class PrestamoGeneradorResumenExcelStrategyImp implements PrestamoGeneradorResumenStrategy{

	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
	
    @Override
    public ByteArrayOutputStream generarResumen(List<PrestamoDto> prestamos) throws IOException {
    	ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet("Prestamos");
        encabezadoExcel(libro, hoja);
        cuerpoExcel(libro, hoja, prestamos);
        ajustarAltoFilas(hoja);
        ajustarAnchoColumnas(hoja);
        libro.write(byteArrayOutputStream);
        libro.close();
        return byteArrayOutputStream;
    }

    private void encabezadoExcel(HSSFWorkbook libro, HSSFSheet hoja) {
        HSSFRow fila = hoja.createRow(1);
        HSSFCell celda;

        celda = fila.createCell(3);
        celda.setCellValue("Resumen de Préstamos de la Biblioteca");
        celda.setCellStyle(estiloTitulo(libro, hoja));

        hoja.addMergedRegion(new CellRangeAddress(1, 1, 3, 9));

        fila = hoja.createRow(3);
        celda = fila.createCell(3);
        celda.setCellValue("Fecha y hora:");
        celda.setCellStyle(estiloEncabezado(libro, hoja));

        celda = fila.createCell(4);
        celda.setCellValue(LocalDateTime.now().format(formatter));
        celda.setCellStyle(estiloFecha(libro));
    }

    private void cuerpoExcel(HSSFWorkbook libro, HSSFSheet hoja, List<PrestamoDto> prestamos) {
        String[] columnas = {"N°", "FECHA Y HORA DE PRÉSTAMO", "FECHA Y HORA DE DEVOLUCIÓN", "FECHA Y HORA DE DEVOLUCIÓN POR MIEMBRO", "NÚM. INV. E ISBN DEL LIBRO", "NÚM. DEL MIEMBRO", "ESTADO","SANCION"};
        HSSFRow fila = hoja.createRow(5);
        HSSFCell celda;

        int nroFila = 6;
        int nroCelda = 1;
        int siguienteNro = 1;

        for (int i = 2; i < 10; i++) {
            celda = fila.createCell(i);
            celda.setCellValue(columnas[i - 2]);
            celda.setCellStyle(estiloCelda(libro));
        }

        for (PrestamoDto p : prestamos) {
            fila = hoja.createRow(nroFila);
            nroCelda++;

            celda = fila.createCell(nroCelda++);
            celda.setCellValue(siguienteNro++);
            celda.setCellStyle(estiloTexto(libro));

            celda = fila.createCell(nroCelda++);
            celda.setCellValue(p.getFechaPrestamo());
            celda.setCellStyle(estiloTexto(libro));

            celda = fila.createCell(nroCelda++);
            celda.setCellValue(p.getFechaDevolucion());
            celda.setCellStyle(estiloTexto(libro));

            celda = fila.createCell(nroCelda++);
            if(p.getFechaDevolucionPorMiembro() == null || p.getFechaDevolucionPorMiembro().isBlank() || p.getFechaDevolucionPorMiembro().isEmpty()) {
            	celda.setCellValue("Aún no se devolvió");
                celda.setCellStyle(estiloInformacion(libro));
        	}else {
                celda.setCellValue(p.getFechaDevolucionPorMiembro());
                celda.setCellStyle(estiloTexto(libro));
        	}

            celda = fila.createCell(nroCelda++);
            celda.setCellValue(p.getLibro().getNumInventario() + " - " + p.getLibro().getIsbn());
            celda.setCellStyle(estiloTexto(libro));

            celda = fila.createCell(nroCelda++);
            celda.setCellValue( p.getMiembro().getNumeroMiembro());
            celda.setCellStyle(estiloTexto(libro));

            celda = fila.createCell(nroCelda++);
            celda.setCellValue(p.getEstadoPrestamo());
            celda.setCellStyle(estiloTexto(libro));
            
            celda = fila.createCell(nroCelda++);
            if (p.getMiembro().getFechaHastaSancion() != null) {
                celda.setCellValue(p.getMiembro().getFechaHastaSancion().format(formatter));
                celda.setCellStyle(estiloTexto(libro));
            } else {
                celda.setCellValue("Sin Sanción");
                celda.setCellStyle(estiloInformacion(libro));
            }


            nroFila++;
            nroCelda = 1;
        }
    }

    private void ajustarAnchoColumnas(HSSFSheet hoja) {
        		hoja.setColumnWidth(2, 588);
        		hoja.setColumnWidth(3, 7205);
        		hoja.setColumnWidth(4, 7792);
        		hoja.setColumnWidth(5, 11409);
        		hoja.setColumnWidth(6, 7792);
        		hoja.setColumnWidth(7, 6994);
        		hoja.setColumnWidth(8, 2280);
        		hoja.setColumnWidth(9, 3500);
    }

    private void ajustarAltoFilas(HSSFSheet hoja) {
        HSSFRow fila = hoja.getRow(1);
        fila.setHeightInPoints(30);
        for (int i = 2; i <= hoja.getLastRowNum(); i++) {
        	fila = hoja.getRow(i);
            if (fila != null) {
                fila.setHeightInPoints(18);
            }
        }
    }

    private CellStyle estiloCelda(HSSFWorkbook libro) {
        Font fuente = libro.createFont();
        CellStyle estilo = libro.createCellStyle();

        estilo.setBorderRight(BorderStyle.MEDIUM);
        estilo.setBorderLeft(BorderStyle.MEDIUM);
        estilo.setBorderTop(BorderStyle.MEDIUM);
        estilo.setBorderBottom(BorderStyle.MEDIUM);

        estilo.setFillBackgroundColor(IndexedColors.BLUE.index);
        estilo.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        estilo.setAlignment(HorizontalAlignment.CENTER);
        estilo.setVerticalAlignment(VerticalAlignment.CENTER);
        
        fuente.setColor(IndexedColors.WHITE.getIndex());
        fuente.setBold(true);
        fuente.setFontName("Consolas");
        fuente.setFontHeight((short) (11 * 20));

        estilo.setFont(fuente);

        return estilo;
    }

    private CellStyle estiloTexto(HSSFWorkbook libro) {
        Font fuente = libro.createFont();
        CellStyle estilo = libro.createCellStyle();

        estilo.setBorderRight(BorderStyle.MEDIUM);
        estilo.setBorderLeft(BorderStyle.MEDIUM);
        estilo.setBorderTop(BorderStyle.MEDIUM);
        estilo.setBorderBottom(BorderStyle.MEDIUM);

        estilo.setAlignment(HorizontalAlignment.CENTER);
        estilo.setVerticalAlignment(VerticalAlignment.CENTER);
        fuente.setColor(IndexedColors.BLACK.getIndex());
        fuente.setFontName("Consolas");
        fuente.setFontHeight((short) (10 * 20));
        estilo.setFont(fuente);

        return estilo;
    }

    private CellStyle estiloTitulo(HSSFWorkbook libro, HSSFSheet hoja) {
        Font fuente = libro.createFont();
        fuente.setColor(IndexedColors.BLACK.getIndex());
        fuente.setBold(true);
        fuente.setFontName("Consolas");
        fuente.setFontHeight((short) (22 * 20));

        CellStyle estilo = libro.createCellStyle();

        estilo.setAlignment(HorizontalAlignment.CENTER);
        estilo.setVerticalAlignment(VerticalAlignment.CENTER);
        estilo.setFillForegroundColor(IndexedColors.AQUA.getIndex());
        estilo.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        estilo.setFont(fuente);

        return estilo;
    }

    private CellStyle estiloEncabezado(HSSFWorkbook libro, HSSFSheet hoja) {
        Font fuente = libro.createFont();
        fuente.setColor(IndexedColors.BLACK.getIndex());
        fuente.setBold(true);
        fuente.setItalic(true);
        fuente.setFontName("Consolas");
        fuente.setFontHeight((short) (12 * 20));

        CellStyle estilo = libro.createCellStyle();
        estilo.setAlignment(HorizontalAlignment.CENTER);
        estilo.setVerticalAlignment(VerticalAlignment.CENTER);
        estilo.setFont(fuente);

        return estilo;
    }

    private CellStyle estiloFecha(HSSFWorkbook libro) {
        Font fuente = libro.createFont();
        fuente.setColor(IndexedColors.AQUA.getIndex());
        fuente.setFontName("Consolas");
        fuente.setFontHeight((short) (11 * 20));
        fuente.setBold(true);
        fuente.setItalic(true);

        CellStyle estilo = libro.createCellStyle();
        estilo.setAlignment(HorizontalAlignment.CENTER);
        estilo.setVerticalAlignment(VerticalAlignment.CENTER);
        estilo.setDataFormat(libro.createDataFormat().getFormat("dd/MM/yyyy HH:mm:ss"));
        estilo.setFont(fuente);

        return estilo;
    }
    
    private CellStyle estiloInformacion(HSSFWorkbook libro) {
        Font fuente = libro.createFont();
        
        fuente.setColor(IndexedColors.GREEN.getIndex());
        fuente.setFontName("Consolas");
        fuente.setFontHeight((short) (11 * 20));

        CellStyle estilo = libro.createCellStyle();
        estilo.setBorderRight(BorderStyle.MEDIUM);
        estilo.setBorderLeft(BorderStyle.MEDIUM);
        estilo.setBorderTop(BorderStyle.MEDIUM);
        estilo.setBorderBottom(BorderStyle.MEDIUM);
        
        estilo.setAlignment(HorizontalAlignment.CENTER);
        estilo.setVerticalAlignment(VerticalAlignment.CENTER);
        estilo.setFont(fuente);

        return estilo;
    }

}
